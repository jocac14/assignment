# Xite Test Assignment

### Run the application

```
sh start.sh
```

### Run the application from your IDE

This project uses project lombok which is a library that helps removing 
the verbosity in the code. In order to run the application in an IDE you need to intall
the lombok plugin and enable the annotations processor in the IDE. For running the application with gradle you
don't need to install anything. For more information visit: https://projectlombok.org/setup/overview