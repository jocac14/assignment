package nl.xite.assignment.exception;

import lombok.extern.slf4j.Slf4j;
import nl.xite.assignment.video.exception.DuplicateVideoIdException;
import nl.xite.assignment.video.exception.VideoNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(DuplicateVideoIdException.class)
    public ResponseEntity handleConflictException(final Exception exception) {
        log.warn(exception.getMessage(), exception);
        return ResponseEntity.status(CONFLICT).body(new ExceptionResource(exception.getMessage()));
    }

    @ExceptionHandler(VideoNotFoundException.class)
    public ResponseEntity handleNotFoundException(final Exception exception) {
        log.warn(exception.getMessage(), exception);
        return ResponseEntity.status(NOT_FOUND).body(new ExceptionResource(exception.getMessage()));
    }
}
