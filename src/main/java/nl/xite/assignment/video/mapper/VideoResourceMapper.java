package nl.xite.assignment.video.mapper;

import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.VideoSubgenre;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.resource.VideoResource;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class VideoResourceMapper {

    public Video toEntity(final VideoResource videoResource) {
        final Video video = Video.builder()
                .id(videoResource.getId())
                .album(videoResource.getAlbum())
                .artist(videoResource.getArtist())
                .duration(videoResource.getDuration())
                .title(videoResource.getTitle())
                .genre(videoResource.getGenre())
                .releaseYear(videoResource.getReleaseYear())
                .build();

        video.setSubgenres(toVideoSubgenres(videoResource.getSubgenres(), video));

        return video;
    }

    public VideoResource toResource(final Video video) {
        return VideoResource.builder()
                .id(video.getId())
                .album(video.getAlbum())
                .artist(video.getArtist())
                .duration(video.getDuration())
                .genre(video.getGenre())
                .title(video.getTitle())
                .releaseYear(video.getReleaseYear())
                .subgenres(toResourceSubgenres(video.getSubgenres()))
                .build();
    }

    private List<VideoSubgenre> toVideoSubgenres(final List<Genre> subgenres, final Video video) {
        return subgenres.stream().map(subgenre -> VideoSubgenre.builder()
                .genre(subgenre)
                .video(video)
                .build()).collect(toList());
    }

    private List<Genre> toResourceSubgenres(final List<VideoSubgenre> subgenres) {
        return subgenres.stream().map(VideoSubgenre::getGenre).collect(toList());
    }
}
