package nl.xite.assignment.video.mapper;

import nl.xite.assignment.video.model.enums.Genre;

import java.beans.PropertyEditorSupport;

public class GenreMapper extends PropertyEditorSupport {
    public void setAsText(final String text) throws IllegalArgumentException {
        super.setValue(Genre.fromText(text));
    }
}
