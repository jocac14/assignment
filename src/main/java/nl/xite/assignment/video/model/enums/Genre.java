package nl.xite.assignment.video.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum Genre {
    BLUES("blues"),
    CLASSICAL("classical"),
    COUNTRY("country"),
    ELECTRONIC("electronic"),
    FOLK("folk"),
    JAZZ("jazz"),
    NEW_AGE("new age"),
    REGGAE("reggae"),
    ROCK("rock");

    private String value;

    Genre(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public static Genre fromText(final String text) {
        return Stream.of(Genre.values())
                .filter(genre -> genre.value.equals(text))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Invalid genre/subgenre value."));
    }
}