package nl.xite.assignment.video.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.xite.assignment.video.model.enums.Genre;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(VideoSubgenre.VideoSubGenrePK.class)
public class VideoSubgenre {

    @Id
    private Video video;

    @Id
    private Genre genre;

    @Data
    public static class VideoSubGenrePK implements Serializable {
        @ManyToOne
        private Video video;

        @Enumerated(EnumType.STRING)
        private Genre genre;
    }
}
