package nl.xite.assignment.video.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.xite.assignment.video.model.enums.Genre;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Video {
    @Id
    private Long id;

    @Column(nullable = false)
    private String title;

    private String album;

    @Column(nullable = false)
    private String artist;

    @Column(nullable = false)
    private int duration;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Genre genre;

    @OneToMany(mappedBy = "video")
    @Cascade(CascadeType.ALL)
    private List<VideoSubgenre> subgenres;

    @Column(nullable = false)
    private int releaseYear;
}
