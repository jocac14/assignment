package nl.xite.assignment.video.repository;

import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.enums.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface VideoRepository extends CrudRepository<Video, Long> {
    @Query("SELECT DISTINCT video FROM Video video JOIN video.subgenres subgenre WHERE " +
            "(:genre is null or video.genre = :genre) and " +
            "(:subgenre is null or subgenre.genre = :subgenre)")
    List<Video> findAll(@Param("genre") final Genre genre,
                        @Param("subgenre") final Genre subgenre);
}
