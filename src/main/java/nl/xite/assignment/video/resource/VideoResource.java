package nl.xite.assignment.video.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.xite.assignment.video.model.enums.Genre;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VideoResource {
    @NotNull
    private long id;

    @NotNull
    @Size(max = 255)
    private String title;

    @Size(max = 255)
    private String album;

    @NotNull
    @Size(max = 255)
    private String artist;

    @NotNull
    private int duration;

    @NotNull
    private Genre genre;

    @NotNull
    private List<Genre> subgenres;

    @Min(1900)
    @Max(2100)
    private int releaseYear;
}
