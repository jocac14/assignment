package nl.xite.assignment.video.service;

import nl.xite.assignment.video.exception.DuplicateVideoIdException;
import nl.xite.assignment.video.exception.VideoNotFoundException;
import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VideoService {

    private final VideoRepository videoRepository;

    @Autowired
    public VideoService(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }

    @Transactional
    public Video create(final Video video) throws DuplicateVideoIdException {
        verifyIdIsUnique(video.getId());
        return videoRepository.save(video);
    }

    @Transactional(rollbackFor = DuplicateVideoIdException.class)
    public Video update(final Long id, final Video video) throws VideoNotFoundException, DuplicateVideoIdException {
        delete(id);
        verifyIdIsUnique(video.getId());
        return videoRepository.save(video);
    }

    @Transactional
    public void delete(final Long id) throws VideoNotFoundException {
        final Video video = get(id);
        videoRepository.delete(video);
    }

    public Video get(final long id) throws VideoNotFoundException {
        return videoRepository.findById(id).orElseThrow(() -> new VideoNotFoundException(id));
    }

    public List<Video> get(final Genre genre, final Genre subgenre) {
        return videoRepository.findAll(genre, subgenre);
    }

    private void verifyIdIsUnique(final long id) throws DuplicateVideoIdException {
        if (videoRepository.existsById(id)) {
            throw new DuplicateVideoIdException(id);
        }
    }
}
