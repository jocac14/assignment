package nl.xite.assignment.video;

import nl.xite.assignment.video.exception.DuplicateVideoIdException;
import nl.xite.assignment.video.exception.VideoNotFoundException;
import nl.xite.assignment.video.mapper.GenreMapper;
import nl.xite.assignment.video.mapper.VideoResourceMapper;
import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.resource.VideoResource;
import nl.xite.assignment.video.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/videos")
public class VideoController {

    private final VideoService videoService;
    private final VideoResourceMapper videoResourceMapper;

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(Genre.class, new GenreMapper());
    }

    @Autowired
    public VideoController(VideoService videoService,
                           VideoResourceMapper videoResourceMapper) {
        this.videoService = videoService;
        this.videoResourceMapper = videoResourceMapper;
    }

    @PostMapping
    public ResponseEntity<VideoResource> create(@Valid @RequestBody VideoResource videoResource) throws DuplicateVideoIdException {
        final Video video = videoService.create(videoResourceMapper.toEntity(videoResource));
        return ResponseEntity.status(CREATED).body(videoResourceMapper.toResource(video));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VideoResource> update(@PathVariable("id") long id, @Valid @RequestBody VideoResource videoResource) throws DuplicateVideoIdException, VideoNotFoundException {
        final Video video = videoService.update(id, videoResourceMapper.toEntity(videoResource));
        return ResponseEntity.status(OK).body(videoResourceMapper.toResource(video));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<VideoResource> delete(@PathVariable("id") long id) throws VideoNotFoundException {
        videoService.delete(id);
        return ResponseEntity.status(NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<VideoResource> get(@PathVariable("id") long id) throws VideoNotFoundException {
        final Video video = videoService.get(id);
        return ResponseEntity.status(OK).body(videoResourceMapper.toResource(video));
    }

    @GetMapping
    public ResponseEntity<List<VideoResource>> get(@RequestParam(name = "genre", required = false) final Genre genre,
                                                   @RequestParam(name = "subgenre", required = false) final Genre subgenre) {
        final List<Video> videos = videoService.get(genre, subgenre);
        final List<VideoResource> videoResources = videos.stream().map(videoResourceMapper::toResource).collect(toList());
        return ResponseEntity.status(OK).body(videoResources);
    }
}
