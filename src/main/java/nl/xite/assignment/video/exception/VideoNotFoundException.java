package nl.xite.assignment.video.exception;

public class VideoNotFoundException extends Exception {
    public VideoNotFoundException(final Long id) {
        super(String.format("Video with id: %d wasn't found.", id));
    }
}
