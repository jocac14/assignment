package nl.xite.assignment.video.exception;

public class DuplicateVideoIdException extends Exception {
    public DuplicateVideoIdException(final Long id) {
        super(String.format("Video with id: %d already exists.", id));
    }
}
