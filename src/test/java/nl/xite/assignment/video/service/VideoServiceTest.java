package nl.xite.assignment.video.service;

import nl.xite.assignment.video.exception.DuplicateVideoIdException;
import nl.xite.assignment.video.exception.VideoNotFoundException;
import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.repository.VideoRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static nl.xite.assignment.factory.VideoFactory.buildVideo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
class VideoServiceTest {

    private static final long VIDEO_ID = 1L;

    @Mock
    private VideoRepository videoRepository;

    @InjectMocks
    private VideoService videoService;

    @Test
    @DisplayName("Create a video with an existent id throws a duplicate video id exception")
    void createWithDuplicateId() {
        final Video createVideoRequest = buildVideo(VIDEO_ID);

        given(videoRepository.existsById(VIDEO_ID)).willReturn(true);

        assertThrows(DuplicateVideoIdException.class, () -> videoService.create(createVideoRequest));

        verify(videoRepository).existsById(VIDEO_ID);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Create a video with a new id returns the created video")
    void createWithValidId() throws DuplicateVideoIdException {
        final Video createVideoRequest = buildVideo(VIDEO_ID);
        final Video expectedCreatedVideo = buildVideo(VIDEO_ID);

        given(videoRepository.existsById(VIDEO_ID)).willReturn(false);
        given(videoRepository.save(createVideoRequest)).willReturn(expectedCreatedVideo);

        final Video createdVideo = videoService.create(createVideoRequest);

        assertThat(createdVideo, is(expectedCreatedVideo));

        verify(videoRepository).existsById(VIDEO_ID);
        verify(videoRepository).save(createVideoRequest);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Update a video that doesn't exist throws a video not found exception")
    void updateWithNotFoundId() {
        final Video updateVideoRequest = buildVideo(VIDEO_ID);

        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.empty());

        assertThrows(VideoNotFoundException.class, () -> videoService.update(VIDEO_ID, updateVideoRequest));

        verify(videoRepository).findById(VIDEO_ID);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Update a video with a duplicate id throws a duplicate video id exception")
    void updateWithDuplicateId() {
        final Video updateVideoRequest = buildVideo(VIDEO_ID);
        final Video existentVideo = buildVideo(VIDEO_ID);

        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.of(existentVideo));
        given(videoRepository.existsById(VIDEO_ID)).willReturn(true);

        assertThrows(DuplicateVideoIdException.class, () -> videoService.update(VIDEO_ID, updateVideoRequest));

        verify(videoRepository).findById(VIDEO_ID);
        verify(videoRepository).delete(existentVideo);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Update a video with a new id returns the updated video")
    void updateWithValidId() throws VideoNotFoundException, DuplicateVideoIdException {
        final Video updateVideoRequest = buildVideo(VIDEO_ID);
        final Video existentVideo = buildVideo(VIDEO_ID);
        final Video expectedUpdatedVideo = buildVideo(VIDEO_ID);

        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.of(existentVideo));
        given(videoRepository.existsById(VIDEO_ID)).willReturn(false);
        given(videoRepository.save(updateVideoRequest)).willReturn(expectedUpdatedVideo);

        final Video updatedVideo = videoService.update(VIDEO_ID, updateVideoRequest);

        assertThat(updatedVideo, is(expectedUpdatedVideo));

        verify(videoRepository).findById(VIDEO_ID);
        verify(videoRepository).delete(existentVideo);
        verify(videoRepository).save(updateVideoRequest);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Delete a video that doesn't exist throws a video not found exception")
    void deleteWithNotFoundId() {
        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.empty());

        assertThrows(VideoNotFoundException.class, () -> videoService.delete(VIDEO_ID));

        verify(videoRepository).findById(VIDEO_ID);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Delete a video that exists removes it from the database")
    void deleteWithValidId() throws VideoNotFoundException {
        final Video video = buildVideo(VIDEO_ID);

        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.of(video));

        videoService.delete(VIDEO_ID);

        verify(videoRepository).findById(VIDEO_ID);
        verify(videoRepository).delete(video);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Get a video that doesn't exist throws a video not found exception")
    void getWithNotFoundId() {
        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.empty());

        assertThrows(VideoNotFoundException.class, () -> videoService.get(VIDEO_ID));

        verify(videoRepository).findById(VIDEO_ID);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Get a video that exists returns the existent video")
    void getWithValidId() throws VideoNotFoundException {
        final Video expectedVideo = buildVideo(VIDEO_ID);

        given(videoRepository.findById(VIDEO_ID)).willReturn(Optional.of(expectedVideo));

        final Video video = videoService.get(VIDEO_ID);

        assertThat(video, is(expectedVideo));
        verify(videoRepository).findById(VIDEO_ID);
        verifyNoMoreInteractions(videoRepository);
    }

    @Test
    @DisplayName("Get all videos returns a list of existent videos")
    void getVideos() {
        final Video expectedVideo = buildVideo(VIDEO_ID);
        final Genre genre = Genre.REGGAE;
        final Genre subgenre = Genre.JAZZ;

        given(videoRepository.findAll(genre, subgenre)).willReturn(singletonList(expectedVideo));

        final List<Video> videos = videoService.get(genre, subgenre);

        assertThat(videos.size(), is(1));
        assertThat(videos, containsInAnyOrder(expectedVideo));
        verify(videoRepository).findAll(genre, subgenre);
        verifyNoMoreInteractions(videoRepository);
    }
}