package nl.xite.assignment.video;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.resource.VideoResource;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static nl.xite.assignment.factory.VideoResourceFactory.buildVideoResource;
import static nl.xite.assignment.video.model.enums.Genre.BLUES;
import static nl.xite.assignment.video.model.enums.Genre.JAZZ;
import static nl.xite.assignment.video.model.enums.Genre.NEW_AGE;
import static nl.xite.assignment.video.model.enums.Genre.ROCK;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class VideoControllerTest {

    private static final long VIDEO_ID = 1L;
    private static final VideoResource VIDEO_RESOURCE = buildVideoResource(VIDEO_ID);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Create a video missing required property returns 400 status code")
    void createVideoWithoutRequiredProperty() throws Exception {
        final VideoResource videoResource = buildVideoResource(VIDEO_ID);
        videoResource.setTitle(null);

        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(videoResource)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Create a video with invalid genre returns 400 status code")
    void createVideoWithInvalidGenre() throws Exception {
        final VideoResource videoResource = buildVideoResource(VIDEO_ID);
        final String request = objectMapper.writeValueAsString(videoResource)
                .replaceAll("reggae", "invalid");

        this.mockMvc.perform(buildCreateRequest(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Create a video with invalid release year returns 400 status code")
    void createVideoWithInvalidReleaseYear() throws Exception {
        final VideoResource videoResource = buildVideoResource(VIDEO_ID);
        videoResource.setReleaseYear(9999);

        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(videoResource)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Create a video with duplicate id returns 409 status code")
    void createVideoWithDuplicateId() throws Exception {
        final String request = objectMapper.writeValueAsString(VIDEO_RESOURCE);
        this.mockMvc.perform(buildCreateRequest(request));
        this.mockMvc.perform(buildCreateRequest(request))
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Create a video with valid request returns 201 status code")
    void createVideo() throws Exception {
        final ResultActions response = this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE)))
                .andExpect(status().isCreated());
        assertResponse(response, VIDEO_RESOURCE);
    }

    @Test
    @DisplayName("Update a video that doesn't exist returns a 404 status code")
    void updateVideoIdNotFound() throws Exception {
        this.mockMvc.perform(buildUpdateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE), 2L))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Update a video with duplicate id returns a 409 status code")
    void updateVideoDuplicateId() throws Exception {
        final long duplicateId = 2L;
        final VideoResource duplicateVideoResource = buildVideoResource(duplicateId);
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(duplicateVideoResource)));
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE)));

        this.mockMvc.perform(buildUpdateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE), duplicateId))
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Update a video with valid id returns a 200 status code")
    void updateVideoWithValidId() throws Exception {
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE)));

        final VideoResource updatedResource = buildVideoResource(VIDEO_ID);
        updatedResource.setTitle("New Title");

        final ResultActions response = this.mockMvc.perform(buildUpdateRequest(objectMapper.writeValueAsString(updatedResource), VIDEO_ID))
                .andExpect(status().isOk());

        assertResponse(response, updatedResource);
    }

    @Test
    @DisplayName("Get a video that doesn't exist returns a 404 status code")
    void getVideoWithNotFoundId() throws Exception {
        this.mockMvc.perform(get("/videos/{id}", VIDEO_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Get a video that exists returns a 200 status code")
    void getVideoWithValidId() throws Exception {
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(VIDEO_RESOURCE)));

        final ResultActions response = this.mockMvc.perform(get("/videos/{id}", VIDEO_ID))
                .andExpect(status().isOk());

        assertResponse(response, VIDEO_RESOURCE);
    }

    @Test
    @DisplayName("Get all videos with and without filters a 200 status code and videos are filtered")
    void getAllVideos() throws Exception {
        final VideoResource rockGenreVideoBluesSubgenre = buildVideo(1L, ROCK, BLUES);
        final VideoResource rockGenreVideoJazzSubgenre = buildVideo(2L, ROCK, JAZZ);
        final VideoResource newAgeVideoJazzSubgenre = buildVideo(3L, NEW_AGE, JAZZ);

        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(rockGenreVideoBluesSubgenre)));
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(rockGenreVideoJazzSubgenre)));
        this.mockMvc.perform(buildCreateRequest(objectMapper.writeValueAsString(newAgeVideoJazzSubgenre)));

        this.mockMvc.perform(get("/videos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));

        this.mockMvc.perform(get("/videos")
                .param("genre", ROCK.getValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));

        this.mockMvc.perform(get("/videos")
                .param("subgenre", BLUES.getValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

        this.mockMvc.perform(get("/videos")
                .param("genre", ROCK.getValue())
                .param("subgenre", JAZZ.getValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    private MockHttpServletRequestBuilder buildCreateRequest(final String request) {
        return post("/videos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request);
    }

    private MockHttpServletRequestBuilder buildUpdateRequest(final String request, final Long id) {
        return put("/videos/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(request);
    }

    private VideoResource buildVideo(final long id, final Genre genre, final Genre subgenre) {
        final VideoResource videoResource = buildVideoResource(id);
        videoResource.setGenre(genre);
        videoResource.setSubgenres(singletonList(subgenre));
        return videoResource;
    }

    private void assertResponse(final ResultActions resultActions,
                                final VideoResource expected) throws Exception {
        final List<String> expectedSubgenres = expected.getSubgenres().stream().map(Genre::getValue).collect(toList());

        resultActions
                .andExpect(jsonPath("id", is((int) expected.getId())))
                .andExpect(jsonPath("title", is(expected.getTitle())))
                .andExpect(jsonPath("album", is(expected.getAlbum())))
                .andExpect(jsonPath("artist", is(expected.getArtist())))
                .andExpect(jsonPath("duration", is(expected.getDuration())))
                .andExpect(jsonPath("genre", is(expected.getGenre().getValue())))
                .andExpect(jsonPath("releaseYear", is(expected.getReleaseYear())))
                .andExpect(jsonPath("subgenres", hasSize(expected.getSubgenres().size())))
                .andExpect(jsonPath("subgenres", containsInAnyOrder(expectedSubgenres.toArray())));
    }
}