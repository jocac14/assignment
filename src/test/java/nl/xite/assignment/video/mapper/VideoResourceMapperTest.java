package nl.xite.assignment.video.mapper;

import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.VideoSubgenre;
import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.resource.VideoResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static nl.xite.assignment.factory.VideoFactory.buildVideo;
import static nl.xite.assignment.factory.VideoResourceFactory.buildVideoResource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;

class VideoResourceMapperTest {

    private static final long VIDEO_ID = 1L;

    private VideoResourceMapper videoResourceMapper;

    @BeforeEach
    void setUp() {
        videoResourceMapper = new VideoResourceMapper();
    }

    @Test
    @DisplayName("Convert video resource to video entity.")
    void toEntity() {
        final VideoResource videoResource = buildVideoResource(VIDEO_ID);
        final Video video = videoResourceMapper.toEntity(videoResource);

        assertThat(video.getId(), is(videoResource.getId()));
        assertThat(video.getTitle(), is(videoResource.getTitle()));
        assertThat(video.getAlbum(), is(videoResource.getAlbum()));
        assertThat(video.getArtist(), is(videoResource.getArtist()));
        assertThat(video.getDuration(), is(videoResource.getDuration()));
        assertThat(video.getGenre(), is(videoResource.getGenre()));
        assertThat(video.getReleaseYear(), is(videoResource.getReleaseYear()));

        final List<Genre> videoSubgenres = video.getSubgenres().stream().map(VideoSubgenre::getGenre).collect(toList());
        final List<Genre> resourceSubgenres = videoResource.getSubgenres();

        assertThat(videoSubgenres.size(), is(resourceSubgenres.size()));
        assertThat(videoSubgenres, containsInAnyOrder(resourceSubgenres.toArray()));
        assertThat(video.getSubgenres().get(0).getVideo(), is(video));
    }

    @Test
    @DisplayName("Convert video entity to video resource.")
    void toResource() {
        final Video video = buildVideo(VIDEO_ID);
        final VideoResource videoResource = videoResourceMapper.toResource(video);

        assertThat(videoResource.getId(), is(video.getId()));
        assertThat(videoResource.getTitle(), is(video.getTitle()));
        assertThat(videoResource.getAlbum(), is(video.getAlbum()));
        assertThat(videoResource.getArtist(), is(video.getArtist()));
        assertThat(videoResource.getDuration(), is(video.getDuration()));
        assertThat(videoResource.getGenre(), is(video.getGenre()));
        assertThat(videoResource.getReleaseYear(), is(video.getReleaseYear()));

        final List<Genre> videoSubgenres = video.getSubgenres().stream().map(VideoSubgenre::getGenre).collect(toList());
        final List<Genre> resourceSubgenres = videoResource.getSubgenres();

        assertThat(resourceSubgenres.size(), is(videoSubgenres.size()));
        assertThat(resourceSubgenres, containsInAnyOrder(videoSubgenres.toArray()));
    }

}