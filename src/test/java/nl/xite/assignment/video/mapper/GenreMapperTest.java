package nl.xite.assignment.video.mapper;

import nl.xite.assignment.video.model.enums.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GenreMapperTest {

    private GenreMapper genreMapper;

    @BeforeEach
    void setUp() {
        genreMapper = new GenreMapper();
    }

    @Test
    @DisplayName("Convert invalid enum value to enum throws IllegalArgumentException")
    void invalidEnumValue() {
        assertThrows(IllegalArgumentException.class, () -> genreMapper.setAsText("invalid"));
    }

    @ParameterizedTest
    @DisplayName("Convert valid enum value to enum returns valid enum")
    @MethodSource("genreMappings")
    void validEnumValue(final String genreText, final Genre expectedGenre) {
        genreMapper.setAsText(genreText);
        assertThat(genreMapper.getValue(), is(expectedGenre));
    }

    private static Stream<Arguments> genreMappings() {
        return Stream.of(
                Arguments.of("blues", Genre.BLUES),
                Arguments.of("classical", Genre.CLASSICAL),
                Arguments.of("country", Genre.COUNTRY),
                Arguments.of("electronic", Genre.ELECTRONIC),
                Arguments.of("folk", Genre.FOLK),
                Arguments.of("jazz", Genre.JAZZ),
                Arguments.of("new age", Genre.NEW_AGE),
                Arguments.of("reggae", Genre.REGGAE),
                Arguments.of("rock", Genre.ROCK)
        );
    }
}