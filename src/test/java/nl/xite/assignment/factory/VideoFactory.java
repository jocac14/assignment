package nl.xite.assignment.factory;

import nl.xite.assignment.video.model.Video;
import nl.xite.assignment.video.model.VideoSubgenre;
import nl.xite.assignment.video.model.enums.Genre;

import static java.util.Arrays.asList;

public class VideoFactory {
    public static Video buildVideo(final long id) {
        return Video.builder()
                .id(id)
                .album("Exodus")
                .artist("Bob Marley")
                .duration(210)
                .genre(Genre.REGGAE)
                .title("One Love")
                .releaseYear(1972)
                .subgenres(asList(buildVideoSubgenre(Genre.BLUES), buildVideoSubgenre(Genre.JAZZ)))
                .build();
    }

    private static VideoSubgenre buildVideoSubgenre(final Genre genre) {
        return VideoSubgenre.builder().genre(genre).build();
    }
}
