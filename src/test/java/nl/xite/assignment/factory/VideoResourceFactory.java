package nl.xite.assignment.factory;

import nl.xite.assignment.video.model.enums.Genre;
import nl.xite.assignment.video.resource.VideoResource;

import static java.util.Arrays.asList;

public class VideoResourceFactory {
    public static VideoResource buildVideoResource(final long id) {
        return VideoResource.builder()
                .id(id)
                .album("Exodus")
                .artist("Bob Marley")
                .duration(210)
                .genre(Genre.REGGAE)
                .title("One Love")
                .releaseYear(1972)
                .subgenres(asList(Genre.BLUES, Genre.JAZZ))
                .build();
    }

}
