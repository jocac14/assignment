package nl.xite.assignment.exception;

import nl.xite.assignment.video.exception.DuplicateVideoIdException;
import nl.xite.assignment.video.exception.VideoNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

class GlobalExceptionHandlerTest {

    private GlobalExceptionHandler globalExceptionHandler;

    @BeforeEach
    void setUp() {
        globalExceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    @DisplayName("Handle duplicate video id exception returns a 409 status code")
    void conflictException() {
        final DuplicateVideoIdException exception = new DuplicateVideoIdException(1L);
        final ResponseEntity responseEntity = globalExceptionHandler.handleConflictException(exception);
        assertThat(responseEntity.getStatusCode(), is(CONFLICT));

        final ExceptionResource exceptionResource = (ExceptionResource) responseEntity.getBody();
        assertThat(exceptionResource.getMessage(), is(exception.getMessage()));
    }

    @Test
    @DisplayName("Handle video not found exception returns a 404 status code")
    void notFoundException() {
        final VideoNotFoundException exception = new VideoNotFoundException(1L);
        final ResponseEntity responseEntity = globalExceptionHandler.handleNotFoundException(exception);
        assertThat(responseEntity.getStatusCode(), is(NOT_FOUND));

        final ExceptionResource exceptionResource = (ExceptionResource) responseEntity.getBody();
        assertThat(exceptionResource.getMessage(), is(exception.getMessage()));
    }
}